package com.bagoflie.dbconnection.Services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.graphics.Color;
import android.os.IBinder;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.bagoflie.dbconnection.InformationStorages.ConnectionUtils;
import com.bagoflie.dbconnection.R;

public class BackgroundConnection extends Service {

    public static boolean connectionStatus = false;
    final String LOG_TAG = "myLogs";

    public BackgroundConnection() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        new Thread() {
            public void run() {
                while (true){
                    connection();
                    statusInform();
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();

        return super.onStartCommand(intent, flags, startId);
    }

    private void connection() {
        if (ConnectionUtils.getConnection()==null) {
            Log.d(LOG_TAG,"FAILURE");
            connectionStatus = false;
        } else {
            Log.d(LOG_TAG, "ACCESS");
            connectionStatus = true;
        }
        Log.d(LOG_TAG, "connection: " + connectionStatus);
    }

    private void statusInform() {
        String outputStatus;
        if (connectionStatus==true) {
            outputStatus = "ACCESS";
        } else {
            outputStatus = "FAILURE";
        }

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(BackgroundConnection.this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(outputStatus)
                        .setContentText("Notification text");

        builder.setColor(Color.BLUE);
        Notification notification = builder.build();

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        notificationManager.notify(1, notification);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
