package com.bagoflie.dbconnection.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bagoflie.dbconnection.Beans.Product;
import com.bagoflie.dbconnection.R;

import java.util.ArrayList;

public class ProductAdapter extends ArrayAdapter<Product> {
    public ProductAdapter(@NonNull Context context, ArrayList<Product> products) {
        super(context, 0, products);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Product product = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.product_item, parent, false);
        }
        TextView productName = convertView.findViewById(R.id.productName);
        TextView productUnit = convertView.findViewById(R.id.productUnit);
        productName.setText(product.getName());
        productUnit.setText(product.getUnit());

        return convertView;
    }
}
