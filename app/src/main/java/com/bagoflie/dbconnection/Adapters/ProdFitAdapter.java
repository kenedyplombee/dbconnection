package com.bagoflie.dbconnection.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bagoflie.dbconnection.Beans.Product_Fitting;
import com.bagoflie.dbconnection.R;

import java.util.ArrayList;

public class ProdFitAdapter extends ArrayAdapter<Product_Fitting> {
    public ProdFitAdapter(@NonNull Context context, ArrayList<Product_Fitting> product_fittings) {
        super(context, 0, product_fittings);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Product_Fitting product_fitting = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.prod_fit_item, parent, false);
        }
        TextView prodFitId = convertView.findViewById(R.id.prod_fit_id);
        TextView productUnit = convertView.findViewById(R.id.prodUnit);
        TextView prodId = convertView.findViewById(R.id.prodId);
        TextView prodName = convertView.findViewById(R.id.prodName);
        TextView prodCount = convertView.findViewById(R.id.prodCount);
        prodFitId.setText(product_fitting.getId());
        productUnit.setText(product_fitting.getProdUnit());
        prodId.setText(product_fitting.getProductId());
        prodName.setText(product_fitting.getProdName());
        prodCount.setText(product_fitting.getProductCount());

        return convertView;
    }
}
