package com.bagoflie.dbconnection.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bagoflie.dbconnection.Beans.Fitting;
import com.bagoflie.dbconnection.Beans.Product;
import com.bagoflie.dbconnection.R;

import java.util.ArrayList;

public class FittingAdapter extends ArrayAdapter<Fitting> {
    public FittingAdapter(@NonNull Context context, ArrayList<Fitting> products) {
        super(context, 0, products);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Fitting product = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.fitting_item, parent, false);
        }
        TextView fittingTime = convertView.findViewById(R.id.fittingTime);
        TextView fittingNumber = convertView.findViewById(R.id.fittingNumber);
        fittingNumber.setText(product.getNumber());
        fittingTime.setText(product.getDateTime());

        return convertView;
    }
}
