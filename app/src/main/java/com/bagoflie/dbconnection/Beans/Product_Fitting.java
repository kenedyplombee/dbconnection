package com.bagoflie.dbconnection.Beans;

public class Product_Fitting {
    private String  id;
    private String  prodName;
    private String  prodUnit;
    private String  productId;
    private String  productCount;

    public Product_Fitting(){}

    public Product_Fitting(String  id, String prodName, String prodUnit, String productId, String productCount) {
        this.id = id;
        this.prodName = prodName;
        this.prodUnit = prodUnit;
        this.productId = productId;
        this.productCount = productCount;
    }

    public String  getId() {
        return id;
    }

    public void setId(String  id) {
        this.id = id;
    }

    public String getProdName() {
        return prodName;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }

    public String getProdUnit() {
        return prodUnit;
    }

    public void setProdUnit(String prodUnit) {
        this.prodUnit = prodUnit;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductCount() {
        return productCount;
    }

    public void setProductCount(String productCount) {
        this.productCount = productCount;
    }
}
