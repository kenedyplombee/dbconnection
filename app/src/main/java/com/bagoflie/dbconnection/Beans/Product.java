package com.bagoflie.dbconnection.Beans;

import androidx.annotation.NonNull;

public class Product {
    private int id;
    private String name;
    private String unit;

    public Product(int id, String name, String unit) {
        this.id = id;
        this.name = name;
        this.unit = unit;
    }
    public Product() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @NonNull
    @Override
    public String toString() {
        return this.getName();
    }
}
