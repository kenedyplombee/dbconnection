package com.bagoflie.dbconnection.Beans;

public class Fitting {

    private String dateTime;
    private String number;
    private int id;

    public Fitting(int id, String dateTime, String number) {
        this.dateTime = dateTime;
        this.number = number;
        this.id = id;
    }

    public Fitting(){}

    public String getDateTime() {
        return dateTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String  getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
