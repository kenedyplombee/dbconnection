package com.bagoflie.dbconnection.InformationStorages;

import com.bagoflie.dbconnection.Forms.MainActivity;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtils {

    private static Connection connection;
    public static String url;
    public static String login;
    public static String password;

    public static Connection getConnection() {
        connection = null;
        try {
            Class.forName(MainActivity.DRIVER_CLASS);
            connection = DriverManager.getConnection(ConnectionUtils.url, ConnectionUtils.login, ConnectionUtils.password);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return connection;
    }
}
