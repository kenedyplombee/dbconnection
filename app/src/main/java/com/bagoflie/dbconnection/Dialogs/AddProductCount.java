package com.bagoflie.dbconnection.Dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.bagoflie.dbconnection.Forms.AddProductToFitForm;
import com.bagoflie.dbconnection.Forms.FittingConstructor;
import com.bagoflie.dbconnection.Forms.FittingForm;
import com.bagoflie.dbconnection.InformationStorages.ConnectionUtils;
import com.bagoflie.dbconnection.R;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class AddProductCount extends AppCompatDialogFragment {

    private EditText count;
    private String productCount;
    private Connection connection = null;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View view = inflater.inflate(R.layout.fit_number_dialog, null);

        count = view.findViewById(R.id.product_count_field);

        builder.setView(view)
                .setTitle("Products count")
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        productCount = count.getText().toString();
                        addToFitting();
                    }
                });
        return builder.create();
    }

    private void addToFitting() {
        try {
            connection = ConnectionUtils.getConnection();

            Log.i("mLog", FittingForm.idOfFitting);
            Log.i("mLog", AddProductToFitForm.productId);
            Log.i("mLog", productCount);

            Statement statement = connection.createStatement();
            statement.executeQuery("insert into PROD_FIT(FIT_ID, PROD_ID, PROD_COUNT) values ("
                    + FittingForm.idOfFitting + ", "
                    + AddProductToFitForm.productId +", "
                    + productCount + ");");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
