package com.bagoflie.dbconnection.Dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.bagoflie.dbconnection.Forms.AddProductToFitForm;
import com.bagoflie.dbconnection.Forms.FittingConstructor;
import com.bagoflie.dbconnection.Forms.FittingForm;
import com.bagoflie.dbconnection.InformationStorages.ConnectionUtils;
import com.bagoflie.dbconnection.R;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class RemoveProductFromFitting extends AppCompatDialogFragment {

    private EditText confirmField;
    private String confirm;
    private Connection connection = null;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View view = inflater.inflate(R.layout.remove_product_from_fitting_dialog, null);

        confirmField = view.findViewById(R.id.confirmField);

        builder.setView(view)
                .setTitle("Remove product")
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        confirm = confirmField.getText().toString();
                        if (confirm.equals("Да") || confirm.equals("Yes")) {
                            deleteProductFromFitting();
                        }
                    }
                });
        return builder.create();
    }

    private void deleteProductFromFitting() {
        try {
            connection = ConnectionUtils.getConnection();

            Statement statement = connection.createStatement();
            statement.executeQuery("delete from PROD_FIT\n" +
                    "where ID = " + FittingConstructor.idProductForDelete);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
