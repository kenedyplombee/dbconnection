package com.bagoflie.dbconnection.Forms;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.EditText;

import com.bagoflie.dbconnection.Services.BackgroundConnection;
import com.bagoflie.dbconnection.InformationStorages.ConnectionUtils;
import com.bagoflie.dbconnection.R;

public class MainActivity extends AppCompatActivity {

    private String userName = "";
    private String password = "";

    private EditText userNameField;
    private EditText passwordField;

    public static final String DRIVER_CLASS = "net.sourceforge.jtds.jdbc.Driver";

    public static final String APP_PREFERENCES = "mySettings";

    public static final String APP_PREFERENCES_LOGIN = "login";

    SharedPreferences mSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSettings = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);

        userNameField = findViewById(R.id.userNameField);
        passwordField = findViewById(R.id.passwordField);

        if (mSettings.contains(APP_PREFERENCES_LOGIN)) {
            userNameField.setText(mSettings.getString(APP_PREFERENCES_LOGIN, ""));
        }

        ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.INTERNET}, PackageManager.PERMISSION_GRANTED);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    public void loginDB(View view) {
        SharedPreferences.Editor editor = mSettings.edit();
        userName = userNameField.getText().toString();
        editor.putString(APP_PREFERENCES_LOGIN, userName);
        editor.apply();
        ConnectionUtils.login = userName;

        password = passwordField.getText().toString();
        ConnectionUtils.password = password;
        ConnectionUtils.url = mSettings.getString(Settings.APP_PREFERENCES_URL, "");

        Intent intent = new Intent(MainActivity.this, BackgroundConnection.class);
        startService(intent);

        startActivity(new Intent(MainActivity.this, Menu.class));
    }

    public void onSettings(View view) {
        Intent intent = new Intent(MainActivity.this, Settings.class);
        startActivity(intent);
    }

}