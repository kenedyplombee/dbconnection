package com.bagoflie.dbconnection.Forms;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.bagoflie.dbconnection.Adapters.FittingAdapter;
import com.bagoflie.dbconnection.Beans.Fitting;
import com.bagoflie.dbconnection.InformationStorages.BufferStorage;
import com.bagoflie.dbconnection.InformationStorages.ConnectionUtils;
import com.bagoflie.dbconnection.R;
import com.bagoflie.dbconnection.Services.BackgroundConnection;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class FittingForm extends AppCompatActivity {

    private ListView fittingList;

    private Menu menu;

    private Connection connection = null;

    public static String numberOfFitting;
    public static String idOfFitting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fitting);

        menu = new Menu();

        fittingList = findViewById(R.id.fittingList);

        fittingList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(view.getContext(), BufferStorage.fittingList.get(position).getNumber(), Toast.LENGTH_SHORT).show();

                numberOfFitting = BufferStorage.fittingList.get(position).getNumber();
                idOfFitting = String.valueOf(BufferStorage.fittingList.get(position).getId());

                startActivity(new Intent(FittingForm.this, FittingConstructor.class));
            }
        });

        if  (BackgroundConnection.connectionStatus)
        {
            showList2(menu.getAllFittingsFromRemoteDB());
        } else {
            showList2(BufferStorage.fittingList);
        }

    }
    private void showList2(ArrayList<Fitting> fittings) {
        FittingAdapter fittingAdapter = new FittingAdapter(this, fittings);
        ListView listView = findViewById(R.id.fittingList);
        listView.setAdapter(fittingAdapter);
    }

    public void onUpdateFittingList(View view) {
        if (BackgroundConnection.connectionStatus) {
            BufferStorage.fittingList.clear();
            showList2(menu.getAllFittingsFromRemoteDB());
        }
    }

    public void onCreateNewFitting(View view) {
        Fitting fit = BufferStorage.fittingList.get(BufferStorage.fittingList.size()-1);
        int numberLastFit = Integer.parseInt(fit.getNumber());
        int nextId = fit.getId() + 1;
        int nextFitNumber = ++numberLastFit;
        numberOfFitting = String.valueOf(nextFitNumber);
        idOfFitting = String.valueOf(nextId);
        Log.i("mLog", fit.getNumber());
        Log.i("mLog", String.valueOf(numberLastFit + 1));
        Date currentTime = Calendar.getInstance().getTime();
        Log.i("mLog", currentTime.toString());

        try {
            connection = ConnectionUtils.getConnection();

            Statement statement = connection.createStatement();
            statement.executeQuery(" insert into FITS (TIME, NUMBER) values ('" + currentTime + "', " + nextFitNumber + ") ");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        startActivity(new Intent(FittingForm.this, FittingConstructor.class));
    }
}