package com.bagoflie.dbconnection.Forms;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.bagoflie.dbconnection.R;

public class Settings extends AppCompatActivity {

    private EditText ipField;
    private EditText portField;
    private EditText nameDatabaseField;

    private String url = "";

    public static final String APP_PREFERENCES_HOST = "host";
    public static final String APP_PREFERENCES_PORT = "port";
    public static final String APP_PREFERENCES_DATABASE = "database";
    public static final String APP_PREFERENCES_URL = "url";

    SharedPreferences mSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        mSettings = getSharedPreferences(MainActivity.APP_PREFERENCES, Context.MODE_PRIVATE);

        ipField = findViewById(R.id.ipField);
        portField = findViewById(R.id.portField);
        nameDatabaseField = findViewById(R.id.nameDatabaseField);

        if (mSettings.contains(APP_PREFERENCES_HOST)) {
            ipField.setText(mSettings.getString(APP_PREFERENCES_HOST, ""));
        }
        if (mSettings.contains(APP_PREFERENCES_PORT)) {
            portField.setText(mSettings.getString(APP_PREFERENCES_PORT, ""));
        }
        if (mSettings.contains(APP_PREFERENCES_DATABASE)) {
            nameDatabaseField.setText(mSettings.getString(APP_PREFERENCES_DATABASE, ""));
        }
    }

    public void saveConfig(View view) {
        SharedPreferences.Editor editor = mSettings.edit();

        String host = ipField.getText().toString();
        String port = portField.getText().toString();
        String databaseName = nameDatabaseField.getText().toString();

        url = "jdbc:jtds:sqlserver://"+host+":"+port+"/"+databaseName;

        editor.putString(APP_PREFERENCES_HOST, host);
        editor.putString(APP_PREFERENCES_PORT, port);
        editor.putString(APP_PREFERENCES_DATABASE, databaseName);
        editor.putString(APP_PREFERENCES_URL, url);
        editor.apply();

        Intent intent = new Intent(Settings.this, MainActivity.class);
        startActivity(intent);
    }
}