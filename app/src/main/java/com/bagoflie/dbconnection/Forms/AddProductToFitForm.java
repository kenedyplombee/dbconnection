package com.bagoflie.dbconnection.Forms;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.bagoflie.dbconnection.Adapters.ProductAdapter;
import com.bagoflie.dbconnection.Beans.Product;
import com.bagoflie.dbconnection.Dialogs.AddProductCount;
import com.bagoflie.dbconnection.InformationStorages.BufferStorage;
import com.bagoflie.dbconnection.InformationStorages.ConnectionUtils;
import com.bagoflie.dbconnection.R;
import com.bagoflie.dbconnection.Services.BackgroundConnection;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class AddProductToFitForm extends AppCompatActivity {

    private ListView productList;
    private EditText idForFind;
    private EditText nameForFind;

    private Connection connection = null;

    private ArrayList<Product> localProducts;

    private Menu menu;

    public static String productId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product_to_fit_form);
        menu = new Menu();

        localProducts = menu.getAllProductsFromRemoteDB();

        idForFind = findViewById(R.id.idForFind);
        nameForFind = findViewById(R.id.nameForFind);


        productList = findViewById(R.id.productListForAdd);

        productList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(view.getContext(), localProducts.get(position).getName(), Toast.LENGTH_SHORT).show();
                productId = String.valueOf(localProducts.get(position).getId());

                AddProductCount addProductCount = new AddProductCount();
                addProductCount.show(getSupportFragmentManager(), "Products count");
            }
        });

        if  (BackgroundConnection.connectionStatus)
        {
            showList2(menu.getAllProductsFromRemoteDB());
        } else {
            showList2(BufferStorage.productList);
        }
    }

    private void showList2(ArrayList<Product> products) {
        ProductAdapter productAdapter = new ProductAdapter(this, products);
        ListView listView = findViewById(R.id.productListForAdd);
        listView.setAdapter(productAdapter);
    }

    public void findProductById(View view) {
        String productId =  idForFind.getText().toString();
        localProducts.clear();


        Statement statement = null;
        try {
            connection = ConnectionUtils.getConnection();

            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from DIRECTORY\n" +
                    " where id = " + productId);
            while (resultSet.next()) {
                Product product = new Product();
                product.setId(Integer.parseInt(resultSet.getString(1)));
                product.setName(resultSet.getString(2));
                product.setUnit(resultSet.getString(3));
                localProducts.add(product);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        showList2(localProducts);
    }

    public void findProductByName(View view) {
        String productName =  nameForFind.getText().toString();
        localProducts.clear();


        Statement statement = null;
        try {
            connection = ConnectionUtils.getConnection();

            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM DIRECTORY\n" +
                    "WHERE\n" +
                    "NAME LIKE '%"+ productName + "%'");
            while (resultSet.next()) {
                Product product = new Product();
                product.setId(Integer.parseInt(resultSet.getString(1)));
                product.setName(resultSet.getString(2));
                product.setUnit(resultSet.getString(3));
                localProducts.add(product);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        showList2(localProducts);
    }
}