package com.bagoflie.dbconnection.Forms;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.bagoflie.dbconnection.Beans.Fitting;
import com.bagoflie.dbconnection.Beans.Product;
import com.bagoflie.dbconnection.Beans.Product_Fitting;
import com.bagoflie.dbconnection.InformationStorages.BufferStorage;
import com.bagoflie.dbconnection.InformationStorages.ConnectionUtils;
import com.bagoflie.dbconnection.R;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Menu extends AppCompatActivity {

    private Connection connection = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        getAllProductsFromRemoteDB();
        getAllFittingsFromRemoteDB();
    }

    public void onDirectory(View view) {
        Intent intent = new Intent(Menu.this, Directory.class);
        startActivity(intent);
    }

    public void onFitting(View view) {
        startActivity(new Intent(Menu.this, FittingForm.class));
    }

    public ArrayList<Product> getAllProductsFromRemoteDB(){
        ArrayList<Product> productList = new ArrayList<>();

        Statement statement = null;
        try {
            connection = ConnectionUtils.getConnection();

            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from DIRECTORY;");
            while (resultSet.next()) {
                Product product = new Product();
                product.setId(Integer.parseInt(resultSet.getString(1)));
                product.setName(resultSet.getString(2));
                product.setUnit(resultSet.getString(3));
                productList.add(product);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        BufferStorage.productList = productList;
        return productList;
    }

    public ArrayList<Fitting> getAllFittingsFromRemoteDB(){
        ArrayList<Fitting> fittingList = new ArrayList<>();

        Statement statement = null;
        try {
            connection = ConnectionUtils.getConnection();

            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from FITS;");
            while (resultSet.next()) {
                Fitting fitting = new Fitting();
                fitting.setId(Integer.parseInt(resultSet.getString(1)));
                fitting.setDateTime(resultSet.getString(2));
                fitting.setNumber(resultSet.getString(3));
                fittingList.add(fitting);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        BufferStorage.fittingList = fittingList;
        return fittingList;
    }


}