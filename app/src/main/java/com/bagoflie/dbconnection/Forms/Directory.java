package com.bagoflie.dbconnection.Forms;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.bagoflie.dbconnection.InformationStorages.BufferStorage;
import com.bagoflie.dbconnection.Services.BackgroundConnection;
import com.bagoflie.dbconnection.Beans.Product;
import com.bagoflie.dbconnection.Adapters.ProductAdapter;
import com.bagoflie.dbconnection.R;

import java.util.ArrayList;

public class Directory extends AppCompatActivity {

    private ListView productList;

    private Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_directory);

        menu = new Menu();

        productList = findViewById(R.id.productList);
//        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Toast.makeText(view.getContext(), ((TextView) view).getText(), Toast.LENGTH_SHORT).show();
//            }
//        });

        productList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(view.getContext(), BufferStorage.productList.get(position).getName(), Toast.LENGTH_SHORT).show();
            }
        });

        if  (BackgroundConnection.connectionStatus)
        {
            showList2(menu.getAllProductsFromRemoteDB());
        } else {
            showList2(BufferStorage.productList);
        }
    }

//    private ArrayList<Product> getAllProductsFromRemoteDB(){
//        ArrayList<Product> productList = new ArrayList<>();
//
//        Statement statement = null;
//        try {
//            connection = ConnectionUtils.getConnection();
//
//            statement = connection.createStatement();
//            ResultSet resultSet = statement.executeQuery("select * from DIRECTORY;");
//            while (resultSet.next()) {
//                Product product = new Product();
//                product.setId(Integer.parseInt(resultSet.getString(1)));
//                product.setName(resultSet.getString(2));
//                product.setUnit(resultSet.getString(3));
//                productList.add(product);
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return productList;
//    }

//    private void showList(ArrayList<Product> products) {
//
//        ArrayAdapter<Product> adapter = new ArrayAdapter<Product>(this,
//                android.R.layout.simple_list_item_1, products);
//
//        productList.setAdapter(adapter);
//    }

    private void showList2(ArrayList<Product> products) {
        ProductAdapter productAdapter = new ProductAdapter(this, products);
        ListView listView = findViewById(R.id.productList);
        listView.setAdapter(productAdapter);
    }

    public void onUpdateProductList(View view) {
        if (BackgroundConnection.connectionStatus) {
            BufferStorage.productList.clear();
            showList2(menu.getAllProductsFromRemoteDB());
        }
    }
}