package com.bagoflie.dbconnection.Forms;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bagoflie.dbconnection.Adapters.FittingAdapter;
import com.bagoflie.dbconnection.Adapters.ProdFitAdapter;
import com.bagoflie.dbconnection.Beans.Fitting;
import com.bagoflie.dbconnection.Beans.Product_Fitting;
import com.bagoflie.dbconnection.Dialogs.RemoveProductFromFitting;
import com.bagoflie.dbconnection.InformationStorages.BufferStorage;
import com.bagoflie.dbconnection.InformationStorages.ConnectionUtils;
import com.bagoflie.dbconnection.R;
import com.bagoflie.dbconnection.Services.BackgroundConnection;

import org.w3c.dom.Text;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class FittingConstructor extends AppCompatActivity {

    private ListView prod_fitList;

    public static String idProductForDelete;

    private String fitNumber;

    private Connection connection = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fitting_constructor);

        prod_fitList = findViewById(R.id.prod_fitList);

        prod_fitList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(view.getContext(), BufferStorage.prod_fitList.get(position).getProdName(), Toast.LENGTH_SHORT).show();

                idProductForDelete = BufferStorage.prod_fitList.get(position).getId();

                Log.i("mLog", idProductForDelete);

                RemoveProductFromFitting removeProductFromFitting = new RemoveProductFromFitting();
                removeProductFromFitting.show(getSupportFragmentManager(), "Remove product");
            }
        });

        if  (BackgroundConnection.connectionStatus)
        {
            showList2(getAllProdFitFromRemoteDB());
        } else {
            showList2(BufferStorage.prod_fitList);
        }
    }

    private void showList2(ArrayList<Product_Fitting> prod_fin) {
        ProdFitAdapter prodFitAdapter = new ProdFitAdapter(this, prod_fin);
        ListView listView = findViewById(R.id.prod_fitList);
        listView.setAdapter(prodFitAdapter);
    }

    public ArrayList<Product_Fitting> getAllProdFitFromRemoteDB(){
        ArrayList<Product_Fitting> product_fittings = new ArrayList<>();

        fitNumber = FittingForm.numberOfFitting;
        String numberFittingString = "Сборка № " + fitNumber;

        TextView textView = findViewById(R.id.testNumberFit);

        textView.setText(numberFittingString);

        Statement statement = null;
        try {
            connection = ConnectionUtils.getConnection();

            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT PROD_FIT.ID, DIRECTORY.ID, DIRECTORY.NAME, PROD_FIT.PROD_COUNT, DIRECTORY.UNIT\n" +
                    "FROM PROD_FIT\n" +
                    "JOIN DIRECTORY ON PROD_FIT.PROD_ID=DIRECTORY.ID\n" +
                    "JOIN FITS ON PROD_FIT.FIT_ID=FITS.ID\n" +
                    "WHERE FITS.NUMBER = " + fitNumber);
            while (resultSet.next()) {
                Product_Fitting product_fitting = new Product_Fitting();
                product_fitting.setId(resultSet.getString(1));
                product_fitting.setProductId(resultSet.getString(2));
                product_fitting.setProdName(resultSet.getString(3));
                product_fitting.setProductCount(resultSet.getString(4));
                product_fitting.setProdUnit(resultSet.getString(5));
                product_fittings.add(product_fitting);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        BufferStorage.prod_fitList = product_fittings;
        return product_fittings;
    }

    public void onAddProductClick(View view) {
        startActivity(new Intent(FittingConstructor.this, AddProductToFitForm.class));
    }

}